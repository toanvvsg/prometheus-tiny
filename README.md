# Prometheus::Tiny
Prometheus Tiny is a simple and easy to integrate gem that exposes Prometheus app performance metrics for Ruby on Rails applications.


## Installation

1. Install the gem and add to the application's Gemfile by executing:
```shell
$ bundle add prometheus-tiny --git https://gitlab.com/toanvvsg/prometheus-tiny.git
$ bundle install
```


2. Update your Rails app's config.ru

```
# config.ru
require_relative 'config/environment'
require 'prometheus/middleware/exporter' # add new

use Prometheus::Middleware::Exporter     # add new

run Rails.application
Rails.application.load_server
```
## Usage
After installing the gem, you can access the metrics endpoint at /metrics.

The gem exposes the following metrics:

- app_requests: A counter for the total number of HTTP requests processed by the app.
- app_response_time: A histogram for the HTTP request duration in seconds.

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).
