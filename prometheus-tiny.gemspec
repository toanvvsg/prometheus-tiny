# frozen_string_literal: true

require_relative "lib/prometheus/tiny/version"

Gem::Specification.new do |spec|
  spec.name = "prometheus-tiny"
  spec.version = Prometheus::Tiny::VERSION
  spec.authors = ["Toan"]
  spec.email = ["toanvvsg@gmail.com"]

  spec.summary = "a simple prometheus exporter."
  spec.description = "a simple prometheus exporter for rails applications."
  spec.homepage = "https://gitlab.com/toanvvsg/prometheus-tiny.git"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["homepage_uri"] = spec.homepage

  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.add_dependency "prometheus-client", "~>4.2"
end
