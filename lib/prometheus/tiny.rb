# frozen_string_literal: true

require_relative "tiny/version"
require_relative "tiny/railtie"

module Prometheus
  module Tiny
    class Error < StandardError; end
  end
end
