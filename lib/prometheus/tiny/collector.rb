# frozen_string_literal: true

require "prometheus/client"

module Prometheus
  module Tiny
    class Collector # no-doc
      def initialize(app)
        @app = app
        init_instruments
      end

      def call(env)
        request_started_on = Time.now
        @status, @headers, @response = @app.call(env)
        request_ended_on = Time.now

        http_labels = {
          path: http_request_path(env),
          method: http_request_method(env)
        }
        @app_response_time.observe(request_ended_on - request_started_on, labels: http_labels)
        @app_requests.increment(labels: http_labels.merge(code: @status))

        [@status, @headers, @response]
      end

      private

      def http_request_path(env)
        env["ORIGINAL_FULLPATH"]
      end

      def http_request_method(env)
        env["REQUEST_METHOD"].downcase
      end

      def registry
        @registry ||= Prometheus::Client.registry
      end

      def init_instruments
        @app_response_time = registry.histogram(
          :app_response_time,
          docstring: "response time of the application",
          labels: %i[path method]
        )
        @app_requests = registry.counter(
          :app_requests,
          docstring: "total requests of the application",
          labels: %i[path method code]
        )
      end
    end
  end
end
