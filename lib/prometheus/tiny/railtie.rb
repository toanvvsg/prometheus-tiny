# frozen_string_literal: true
require_relative "./collector"
 
module Prometheus::Tiny
  class Railtie < ::Rails::Railtie
    initializer "prometheus_tiny.middleware" do |app|
      app.middleware.use Collector
    end
  end
end
